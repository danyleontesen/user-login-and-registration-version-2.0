<?php

    include_once('App/User.php');
    include_once('App/UserAuth.php');
    
    $data = json_decode(file_get_contents("php://input"));

    if(isset($_SESSION['email'])){
        
        header('location: Views/Home.php');

    } else if(isset($data->method)) {

        // Acceso de usuario
        if($data->method == 'login_user') {

            $email = $data->data->email;
            $password = $data->data->password;
            
            $userAuth = new UserAuth;
            $validation = $userAuth->loginFieldsValidate($email, $password);

            // Si no hay errores de validación
            if(sizeof($validation)==0) {
                
                // Ingresa
                $toLogin = $userAuth->loginUser($email, $password);
                
                // Si es true : Ingreso de usuario correctamente
                if($toLogin === true){
                    echo true;
                } else {
                    // Datos incorrectos
                    echo json_encode($toLogin, true);
                }            
            } else {
                // Errores de validación
                echo json_encode($validation, true);
            }
            
        } else  // Registro de usuarios 
            if ($data->method == 'register_user') {

            $name = $data->data->name;
            $email = $data->data->email;
            $password = $data->data->password;
            $confirm_password = $data->data->confirm_password;

            $user = new User;
            $validation = $user->registerFieldsValidation($name, $email, $password, $confirm_password);

            // Si no hay errores de validación
            if(sizeof($validation)==0) {

                //Si no está registrado el correo
                if(!$user->userExists($email)){
                    
                    // Registra
                    $toRegister = $user->registerUser($name, $email, $password);

                    // Registro de usuario exitoso
                    if ($toRegister === true) {
                        echo true;
                    } else {
                        // Problemas al guardar usuario
                        $errors = ['Error al crear cuenta, inténtelo nuevamente.'];
                        echo json_encode($toRegister, true);   
                    }
                } else {
                    // Correo existe en la bd
                    $errors = ['Usuario existente, intente con otro correo.'];
                    echo json_encode($errors, true);
                }
            } else {
                // Errores de validación
                echo json_encode($validation, true);
            }

        } else // Cerrar sesión de usuario
            if ($data->method == 'logout_user') {

            $userAuth = new UserAuth;
            $userAuth->logoutUser();

        }
    } else {
        header('location: Views/Welcome.php');
    }




