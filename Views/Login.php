    <?php 

        require('Partials/header.php');

        // Crea o renauda la sesión
        session_start();
        
        // Si existe variable se sesión, hay sesión activa y redireccione a home
        if(isset($_SESSION['status'])) {
            header('location: Home.php');
            echo $_SESSION['status'];
        }
    ?>

        <div class="app-content">
            <div class="login__title pb-5">
                <h3 class="text-center">Login</h3>
            </div>
            <div class="row justify-content-center">
                <form action="" method="POST" class="form challenge__form col-4" id="login-form">
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control" required autofocus>
                        <label for="email">Email</label>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" required>
                        <label for="password">Password</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-login">Login</button>
                    </div>
                    <div class="login__errors">
                        <ul id="login-errors" class="list-group">
                            <!--Errors here-->
                        </ul>
                    </div>
                </form>
            </div>
        </div>

    <?php include_once('Partials/Footer.php')  ?>