<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../packages/bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/main.css">

    <title>by Dany Dandy!</title>
</head>
<body>

    <header class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a href="Welcome.php">Home</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <?php
                        // Si no hay sesión activa
                        if(!isset($_SESSION['status'])) {   
                    ?>
                        <li class="nav-item">
                            <a href="Register.php" class="nav-link">Register</a>
                        </li>
                        <li class="nav-item">
                            <a href="Login.php" class="nav-link">Login</a>
                        </li>
                    <?php
                        } else {
                        
                            // Si hay sesión activa
                    ?>
                        <li class="nav-item">
                            <a href="" class="nav-link" id="btn-logout">Logout</a>
                        </li>
                    <?php

                        }
                    ?>
                </ul>
            </div>
        </nav> 
    </header>
    
    <main class="container">