<?php

    require_once 'config/Db.php';

    class UserAuth {
        
        private $db;

        public function __construct()
        {
            session_start();
            $this->db = Dbconnection::DB();
        }

        public function loginFieldsValidate($email, $password)
        {
            
            $errors = [];

            // Verificamos que los campos no esten vacíos
            if((!isset($email) || trim($email)=="") || 
                (!isset($password) || trim($password)=="")){
                
                    array_push($errors, "Complete todos los campos");
                    return $errors;

            } else {

                // Validación de correo
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    array_push($errors, "Correo no válido");
                }
                
                //Validación de contraseña
                if(strlen(trim($password)) < 6){
                    array_push($errors, "Contraseña debe ser mayor a 6 caracteres");
                }

                return $errors;
            }
        }
        
        public function loginUser($email, $password)
        {   
            $query = 'SELECT * FROM users WHERE email = :email';
            
            $stm = $this->db->prepare($query);
            $stm->execute(array(':email' => $query, ($email)));

            $result = $stm->fetchAll(PDO::FETCH_ASSOC);

            $stm = null;

            $errors = [];

            //Si existe resultado de la consulta
            if(!empty($result)) {
                //Comparamos contraseñas
                if(password_verify(($password), $result[0]['password']) && $result[0]['status'] == 1){

                    // Establecemos variables de sesión
                    $_SESSION['status'] = 1;    // Estado 1: Sessión activa
                    $_SESSION['name'] =  $result[0]['name'];
                    $_SESSION['email'] = $email;
                    
                    return true;

                } else {
                    // Contraseña incorrecta
                    array_push($errors, "Contraseña incorrecta");
                    
                    return $errors;
                }
            } else {
                // Mensaje de usuario no encontrado en BD
                array_push($errors, "Usuario no registrado");

                return $errors;
            }


        }

        public function logoutUser()
        {
            session_start();
            
            session_unset();
            session_destroy();
        }
    }