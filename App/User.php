<?php

    require_once 'config/Db.php';

    class User  {

        private $db;

        public function __construct()
        {
            session_start();
            $this->db = Dbconnection::DB();
        }

        public function registerFieldsValidation($name, $email, $password, $confirm_password)
        {
            
            $errors = [];

            // Verificamos si los campos estan vacíos
            if((!isset($name) || trim($name)=="") ||
                (!isset($email) || trim($email)=="") ||
                (!isset($password) || trim($password)=="") ||
                (!isset($confirm_password) || trim($confirm_password)=="")){
                
                    array_push($errors, 'Complete todos los campos porfavor.');
                    return $errors;
                
            } else {

                // Validación de correo
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    array_push($errors, 'Correo No válido.');
                }

                // Validamos contraseña mayor a 6 caracteres
                if(strlen(trim($password)) < 6){
                    array_push($errors, 'Contraseña debe contener al menos 6 caracteres.');
                }

                // Validamos confirmación de contraseña
                if(trim($password) != trim($confirm_password)){
                    array_push($errors, 'Contraseñas no coinciden.');
                }
                
                return $errors;
            }
        }
        
        public function userExists($email)
        {
            $query = 'SELECT * FROM users where email = :email';

            $stm = $this->db->prepare($query);
            $stm->execute(['email' => $email]);

            // Si existen registros
            if($stm->rowCount()){
                return true;    // Correo existe
            } else {
                return false;   // Correo no existe, puede ser usado
            }
        }
        
        public function registerUser($name, $email, $password)
        {
            // Crea nuevo usuario
            $query = 'INSERT INTO users (name, email, password, status) VALUES (:name, :email, :password, :status)';

            $stm = $this->db->prepare($query);
            $stm->execute(array(
                ':name' => $query, ($name),
                ':email' => $query, ($email),
                ':password' => $query, (password_hash($password, PASSWORD_DEFAULT)),
                ':status'   => $query, (1)
            ));

            $result = $stm->fetchAll(PDO::FETCH_ASSOC);

            $stm = null;

            // Establecemos variables de sesión
            $_SESSION['status'] = 1;
            $_SESSION['name'] = $name;
            $_SESSION['email'] = $email;
            
            return true;
        }

    }

?>