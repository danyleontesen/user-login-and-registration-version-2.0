<?php

class Dbconnection {

    private static $DB;
    
    public function DB() 
    {
        try {

            // Opciones de conexión
            $arrOptions = array(
                PDO::ATTR_EMULATE_PREPARES => FALSE,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
            );

            // Si no hay una coonexión establecida
            if(!self::$DB){
                $pdo = new PDO(
                    'mysql:host=localhost;dbname=challenge;charset=utf8',
                    'root',
                    'ROOT',
                    $arrOptions
                );
                self::$DB = $pdo;
            }

            return self::$DB;

        } catch (PDOException $e) {
            echo 'Error : ¡ '. $e->getMessage() . ' !';
        }
    }

}

?>