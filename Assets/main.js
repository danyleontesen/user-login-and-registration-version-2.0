
// Login users

$('#login-form').submit(function(e) {
    e.preventDefault();
    
    const data = {
        email           : document.getElementById('email').value,
        password        : document.getElementById('password').value
    }

    axios.post('../Index.php',{
        method: 'login_user',
        data: data
    }).then((response)=> {
        // Si es true o 1, inicia sesión y envía a Home
        if(response.data == 1){
            window.location = '../Views/Home.php';
        } else {
            var list_errors = $('#login-errors')

            list_errors.empty()
            
            // Listado de errores de validación
            response.data.forEach(element => {
                list_errors.append(
                (
                    $('<li>', {
                        'class': "list-group-item",
                        'text': element
                    }))
                )         
            });  
        }
    })

    
})


// Register users

$('#register-form').submit(function(e) {
    e.preventDefault();
    
    const data = {
        name            : document.getElementById('name').value,
        email           : document.getElementById('email').value,
        password        : document.getElementById('password').value,
        confirm_password: document.getElementById('confirm_password').value
    }

    axios.post('../Index.php',{
        method: 'register_user',
        data: data
    }).then((response)=> {
        // Si es true o 1, crea el usuario e inicia sesión
        if(response.data == 1){
            window.location = '../Views/Home.php';
        } else {

            var list_errors = $('#register-errors')

            list_errors.empty()
            
            // Listado de errores de validación
            response.data.forEach(element => {
                list_errors.append(
                (
                    $('<li>', {
                        'class': "list-group-item",
                        'text': element
                    }))
                )         
            });   
        }
    })
})


// Logout user

var btn_logout = $('#btn-logout');

btn_logout.bind('click', function(e){

    e.preventDefault();

    axios.post('../Index.php',{
        method: 'logout_user',
        data: null
    }).then((response)=> {
        window.location = '../Views/Login.php';
    })
})
